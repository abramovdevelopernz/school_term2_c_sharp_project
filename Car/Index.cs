﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class Index : Form
    {


           public Index()
        {
            InitializeComponent();

            
        }

        private void ButtonCustomerRegistration_Click(object sender, EventArgs e)
        {
        
        }

        private void ButtonCarRegistration_Click(object sender, EventArgs e)
        {
         
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void userToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Car form = new Car();
            form.ShowDialog(this);
        }

        private void ButtonOrder_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Colorz form = new Colorz();
            form.ShowDialog(this);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Customer form = new Customer();
            form.ShowDialog(this);
        }

        private void typeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Typez form = new Typez();
            form.ShowDialog(this);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Make form = new Make();
            form.ShowDialog(this);
        }

        private void Index_Activated(object sender, EventArgs e)
        {
            
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            Login form = new Login();
            form.ShowDialog(this);

            if (Globals.pUzrRole == "1")
            {
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                toolStripMenuItem3.Enabled = true;
                toolStripMenuItem4.Enabled = true;
                typeToolStripMenuItem.Enabled = true;
                userToolStripMenuItem2.Enabled = true;

            }
            if (Globals.pUzrRole == "2")
            {
                toolStripMenuItem1.Enabled = true;
                toolStripMenuItem2.Enabled = true;
                toolStripMenuItem3.Enabled = false;
                toolStripMenuItem4.Enabled = false;
                typeToolStripMenuItem.Enabled = false;
                userToolStripMenuItem2.Enabled = false;

            }


        }

        private void Index_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Order form = new Order();
            form.ShowDialog(this);
        }

        private void exitToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void userToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Uzr form = new Uzr();
            form.ShowDialog(this);
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }
    }


}
