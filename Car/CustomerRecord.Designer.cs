﻿namespace Car
{
    partial class CustomerRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAction = new System.Windows.Forms.Button();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.paneMain = new System.Windows.Forms.Panel();
            this.TextBoxFullName = new System.Windows.Forms.TextBox();
            this.TextBoxPhone = new System.Windows.Forms.TextBox();
            this.TextBoxAddress = new System.Windows.Forms.TextBox();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.TextBoxEmail = new System.Windows.Forms.TextBox();
            this.LabelPhone = new System.Windows.Forms.Label();
            this.LabelAddress = new System.Windows.Forms.Label();
            this.LabelDateOfBirth = new System.Windows.Forms.Label();
            this.LabelGender = new System.Windows.Forms.Label();
            this.LabelEmail = new System.Windows.Forms.Label();
            this.LabelFullName = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panelBottom.SuspendLayout();
            this.paneMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAction
            // 
            this.buttonAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAction.Location = new System.Drawing.Point(190, 5);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(90, 33);
            this.buttonAction.TabIndex = 3;
            this.buttonAction.Text = "Action";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBottom.Controls.Add(this.buttonAction);
            this.panelBottom.Controls.Add(this.buttonClose);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 208);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(374, 45);
            this.panelBottom.TabIndex = 14;
            this.panelBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBottom_Paint);
            // 
            // buttonClose
            // 
            this.buttonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonClose.Location = new System.Drawing.Point(3, 5);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(90, 33);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // paneMain
            // 
            this.paneMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneMain.Controls.Add(this.dateTimePicker1);
            this.paneMain.Controls.Add(this.TextBoxFullName);
            this.paneMain.Controls.Add(this.TextBoxPhone);
            this.paneMain.Controls.Add(this.TextBoxAddress);
            this.paneMain.Controls.Add(this.comboBoxGender);
            this.paneMain.Controls.Add(this.TextBoxEmail);
            this.paneMain.Controls.Add(this.LabelPhone);
            this.paneMain.Controls.Add(this.LabelAddress);
            this.paneMain.Controls.Add(this.LabelDateOfBirth);
            this.paneMain.Controls.Add(this.LabelGender);
            this.paneMain.Controls.Add(this.LabelEmail);
            this.paneMain.Controls.Add(this.LabelFullName);
            this.paneMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneMain.Location = new System.Drawing.Point(0, 0);
            this.paneMain.Name = "paneMain";
            this.paneMain.Size = new System.Drawing.Size(374, 253);
            this.paneMain.TabIndex = 15;
            this.paneMain.Paint += new System.Windows.Forms.PaintEventHandler(this.paneMain_Paint);
            // 
            // TextBoxFullName
            // 
            this.TextBoxFullName.Location = new System.Drawing.Point(118, 11);
            this.TextBoxFullName.Name = "TextBoxFullName";
            this.TextBoxFullName.Size = new System.Drawing.Size(233, 20);
            this.TextBoxFullName.TabIndex = 25;
            this.TextBoxFullName.TextChanged += new System.EventHandler(this.TextBoxFullName_TextChanged);
            // 
            // TextBoxPhone
            // 
            this.TextBoxPhone.Location = new System.Drawing.Point(119, 122);
            this.TextBoxPhone.Name = "TextBoxPhone";
            this.TextBoxPhone.Size = new System.Drawing.Size(233, 20);
            this.TextBoxPhone.TabIndex = 23;
            this.TextBoxPhone.TextChanged += new System.EventHandler(this.TextBoxPhone_TextChanged);
            this.TextBoxPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxPhone_KeyPress);
            // 
            // TextBoxAddress
            // 
            this.TextBoxAddress.Location = new System.Drawing.Point(119, 100);
            this.TextBoxAddress.Name = "TextBoxAddress";
            this.TextBoxAddress.Size = new System.Drawing.Size(233, 20);
            this.TextBoxAddress.TabIndex = 22;
            this.TextBoxAddress.TextChanged += new System.EventHandler(this.TextBoxAddress_TextChanged);
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Location = new System.Drawing.Point(119, 56);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(233, 21);
            this.comboBoxGender.TabIndex = 20;
            this.comboBoxGender.SelectedIndexChanged += new System.EventHandler(this.comboBoxGender_SelectedIndexChanged);
            // 
            // TextBoxEmail
            // 
            this.TextBoxEmail.Location = new System.Drawing.Point(118, 34);
            this.TextBoxEmail.Name = "TextBoxEmail";
            this.TextBoxEmail.Size = new System.Drawing.Size(234, 20);
            this.TextBoxEmail.TabIndex = 19;
            this.TextBoxEmail.TextChanged += new System.EventHandler(this.TextBoxEmail_TextChanged);
            // 
            // LabelPhone
            // 
            this.LabelPhone.AutoSize = true;
            this.LabelPhone.Location = new System.Drawing.Point(74, 125);
            this.LabelPhone.Name = "LabelPhone";
            this.LabelPhone.Size = new System.Drawing.Size(38, 13);
            this.LabelPhone.TabIndex = 17;
            this.LabelPhone.Text = "Phone";
            this.LabelPhone.Click += new System.EventHandler(this.LabelPhone_Click);
            // 
            // LabelAddress
            // 
            this.LabelAddress.AutoSize = true;
            this.LabelAddress.Location = new System.Drawing.Point(67, 103);
            this.LabelAddress.Name = "LabelAddress";
            this.LabelAddress.Size = new System.Drawing.Size(45, 13);
            this.LabelAddress.TabIndex = 16;
            this.LabelAddress.Text = "Address";
            this.LabelAddress.Click += new System.EventHandler(this.LabelAddress_Click);
            // 
            // LabelDateOfBirth
            // 
            this.LabelDateOfBirth.AutoSize = true;
            this.LabelDateOfBirth.Location = new System.Drawing.Point(44, 82);
            this.LabelDateOfBirth.Name = "LabelDateOfBirth";
            this.LabelDateOfBirth.Size = new System.Drawing.Size(68, 13);
            this.LabelDateOfBirth.TabIndex = 15;
            this.LabelDateOfBirth.Text = "Date Of Birth";
            this.LabelDateOfBirth.Click += new System.EventHandler(this.LabelDateOfBirth_Click);
            // 
            // LabelGender
            // 
            this.LabelGender.AutoSize = true;
            this.LabelGender.Location = new System.Drawing.Point(70, 59);
            this.LabelGender.Name = "LabelGender";
            this.LabelGender.Size = new System.Drawing.Size(42, 13);
            this.LabelGender.TabIndex = 14;
            this.LabelGender.Text = "Gender";
            this.LabelGender.Click += new System.EventHandler(this.LabelGender_Click);
            // 
            // LabelEmail
            // 
            this.LabelEmail.AutoSize = true;
            this.LabelEmail.Location = new System.Drawing.Point(80, 37);
            this.LabelEmail.Name = "LabelEmail";
            this.LabelEmail.Size = new System.Drawing.Size(32, 13);
            this.LabelEmail.TabIndex = 13;
            this.LabelEmail.Text = "Email";
            this.LabelEmail.Click += new System.EventHandler(this.LabelEmail_Click);
            // 
            // LabelFullName
            // 
            this.LabelFullName.AutoSize = true;
            this.LabelFullName.Location = new System.Drawing.Point(58, 14);
            this.LabelFullName.Name = "LabelFullName";
            this.LabelFullName.Size = new System.Drawing.Size(54, 13);
            this.LabelFullName.TabIndex = 12;
            this.LabelFullName.Text = "Full Name";
            this.LabelFullName.Click += new System.EventHandler(this.LabelFullName_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(119, 78);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(233, 20);
            this.dateTimePicker1.TabIndex = 26;
            // 
            // CustomerRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 253);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.paneMain);
            this.Name = "CustomerRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CustomerRecord";
            this.Load += new System.EventHandler(this.CustomerRecord_Load);
            this.panelBottom.ResumeLayout(false);
            this.paneMain.ResumeLayout(false);
            this.paneMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel paneMain;
        private System.Windows.Forms.Label LabelPhone;
        private System.Windows.Forms.Label LabelAddress;
        private System.Windows.Forms.Label LabelDateOfBirth;
        private System.Windows.Forms.Label LabelGender;
        private System.Windows.Forms.Label LabelEmail;
        private System.Windows.Forms.Label LabelFullName;
        private System.Windows.Forms.TextBox TextBoxAddress;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.TextBox TextBoxEmail;
        private System.Windows.Forms.TextBox TextBoxFullName;
        private System.Windows.Forms.TextBox TextBoxPhone;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}