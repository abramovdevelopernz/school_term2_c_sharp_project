﻿namespace Car
{
    partial class CarRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBottom = new System.Windows.Forms.Panel();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.paneMain = new System.Windows.Forms.Panel();
            this.richTextBoxNotes = new System.Windows.Forms.RichTextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.comboBoxTypez = new System.Windows.Forms.ComboBox();
            this.comboBoxColorz = new System.Windows.Forms.ComboBox();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.comboBoxMakez = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelBottom.SuspendLayout();
            this.paneMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBottom.Controls.Add(this.buttonAction);
            this.panelBottom.Controls.Add(this.buttonClose);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 249);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(293, 45);
            this.panelBottom.TabIndex = 12;
            this.panelBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBottom_Paint);
            // 
            // buttonAction
            // 
            this.buttonAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAction.Location = new System.Drawing.Point(190, 5);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(90, 33);
            this.buttonAction.TabIndex = 3;
            this.buttonAction.Text = "Action";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonClose.Location = new System.Drawing.Point(3, 5);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(90, 33);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // paneMain
            // 
            this.paneMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneMain.Controls.Add(this.richTextBoxNotes);
            this.paneMain.Controls.Add(this.textBoxYear);
            this.paneMain.Controls.Add(this.comboBoxTypez);
            this.paneMain.Controls.Add(this.comboBoxColorz);
            this.paneMain.Controls.Add(this.textBoxModel);
            this.paneMain.Controls.Add(this.comboBoxMakez);
            this.paneMain.Controls.Add(this.label6);
            this.paneMain.Controls.Add(this.label5);
            this.paneMain.Controls.Add(this.label4);
            this.paneMain.Controls.Add(this.label3);
            this.paneMain.Controls.Add(this.label2);
            this.paneMain.Controls.Add(this.label1);
            this.paneMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneMain.Location = new System.Drawing.Point(0, 0);
            this.paneMain.Name = "paneMain";
            this.paneMain.Size = new System.Drawing.Size(293, 249);
            this.paneMain.TabIndex = 13;
            this.paneMain.Paint += new System.Windows.Forms.PaintEventHandler(this.paneMain_Paint);
            // 
            // richTextBoxNotes
            // 
            this.richTextBoxNotes.Location = new System.Drawing.Point(47, 133);
            this.richTextBoxNotes.Name = "richTextBoxNotes";
            this.richTextBoxNotes.Size = new System.Drawing.Size(233, 96);
            this.richTextBoxNotes.TabIndex = 23;
            this.richTextBoxNotes.Text = "";
            this.richTextBoxNotes.TextChanged += new System.EventHandler(this.richTextBoxNotes_TextChanged);
            // 
            // textBoxYear
            // 
            this.textBoxYear.Location = new System.Drawing.Point(48, 107);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(233, 20);
            this.textBoxYear.TabIndex = 22;
            this.textBoxYear.TextChanged += new System.EventHandler(this.textBoxYear_TextChanged);
            // 
            // comboBoxTypez
            // 
            this.comboBoxTypez.FormattingEnabled = true;
            this.comboBoxTypez.Location = new System.Drawing.Point(48, 83);
            this.comboBoxTypez.Name = "comboBoxTypez";
            this.comboBoxTypez.Size = new System.Drawing.Size(233, 21);
            this.comboBoxTypez.TabIndex = 21;
            this.comboBoxTypez.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypez_SelectedIndexChanged);
            // 
            // comboBoxColorz
            // 
            this.comboBoxColorz.FormattingEnabled = true;
            this.comboBoxColorz.Location = new System.Drawing.Point(48, 60);
            this.comboBoxColorz.Name = "comboBoxColorz";
            this.comboBoxColorz.Size = new System.Drawing.Size(233, 21);
            this.comboBoxColorz.TabIndex = 20;
            this.comboBoxColorz.SelectedIndexChanged += new System.EventHandler(this.comboBoxColorz_SelectedIndexChanged);
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(48, 37);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(233, 20);
            this.textBoxModel.TabIndex = 19;
            this.textBoxModel.TextChanged += new System.EventHandler(this.textBoxModel_TextChanged);
            // 
            // comboBoxMakez
            // 
            this.comboBoxMakez.FormattingEnabled = true;
            this.comboBoxMakez.Location = new System.Drawing.Point(48, 12);
            this.comboBoxMakez.Name = "comboBoxMakez";
            this.comboBoxMakez.Size = new System.Drawing.Size(233, 21);
            this.comboBoxMakez.TabIndex = 18;
            this.comboBoxMakez.SelectedIndexChanged += new System.EventHandler(this.comboBoxMakez_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Notes";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Year";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Type";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Color";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Model";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Make";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // CarRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 294);
            this.ControlBox = false;
            this.Controls.Add(this.paneMain);
            this.Controls.Add(this.panelBottom);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CarRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CarRecord";
            this.Load += new System.EventHandler(this.CarRecord_Load);
            this.panelBottom.ResumeLayout(false);
            this.paneMain.ResumeLayout(false);
            this.paneMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel paneMain;
        private System.Windows.Forms.RichTextBox richTextBoxNotes;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.ComboBox comboBoxTypez;
        private System.Windows.Forms.ComboBox comboBoxColorz;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.ComboBox comboBoxMakez;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}