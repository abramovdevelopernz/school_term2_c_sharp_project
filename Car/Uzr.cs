﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class Uzr : Form
    {
        protected void ListviewResize(System.Windows.Forms.ListView pControl)
        {
            CarRecord form = new CarRecord();
            int pListviewWidth = pControl.Width;
            int pColumns = pControl.Columns.Count;

            int pOneColWidth = pListviewWidth / pColumns;

            for (int i = 0; i < pColumns - 1; i++)
            {
                pControl.Columns[i].Width = pOneColWidth;
            }

            pControl.Columns[0].Width = 50;
            pControl.Columns[pColumns - 1].Width = pControl.Columns[pColumns - 1].Width + pOneColWidth - 50;
        }

        public Uzr()
        {
            InitializeComponent();

            //if (listView1.SelectedItems.Count == 0)
            {
                buttonDelete.Enabled = false;
                buttonUpdate.Enabled = false;
            };
            LoadListview1();

           
        }

        private void LoadListview1()
        {

            Globals.pSqlString = "select uzr.id as uzr_id, uzr.name,uzr.email, uzr.pass, uzr.rolez, uzr.deleted from uzr where uzr.deleted=0";


            listView1.Items.Clear();
            listView1.Refresh();
            listView1.View = View.Details;
            listView1.FullRowSelect = true;
            SqlConnection conn = new SqlConnection(Globals.connstr);
            SqlDataAdapter ada = new SqlDataAdapter(Globals.pSqlString, conn);
            DataTable dt = new DataTable();
            ada.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["uzr_id"].ToString());
                listitem.SubItems.Add(dr["name"].ToString());
                listitem.SubItems.Add(dr["email"].ToString());
                listitem.SubItems.Add(dr["pass"].ToString());
                if (dt.Rows[0]["rolez"].ToString() == "1")
                {
                    listitem.SubItems.Add ("Administrator");

                };


                if (dt.Rows[0]["rolez"].ToString() == "2")
                {
                    listitem.SubItems.Add("Assistant");

                };

                listView1.Items.Add(listitem);
            }
            if (dt.Rows.Count > 0)
            {
                listView1.Items[dt.Rows.Count - 1].Selected = true;
            }
            toolStripStatusLabel1.Text = "Total record count : " + dt.Rows.Count.ToString();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            UzrRecord form = new UzrRecord();
            Globals.pCancelAction = false;
            form.Text = "Insert record";
            Globals.pCancelAction = false;
            form.ShowDialog(this);
            if (Globals.pCancelAction == false)
            {
                LoadListview1();
            };
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            UzrRecord form = new UzrRecord();
            Globals.pCancelAction = false;
            Globals.pUzrID = listView1.SelectedItems[0].SubItems[0].Text;
            form.Text = "Edit record";
            form.ShowDialog(this);

            if (Globals.pCancelAction == false)
            {
                listView1.SelectedItems[0].SubItems[1].Text = Globals.pUzrName;
                listView1.SelectedItems[0].SubItems[2].Text = Globals.pUzrEmail;
                listView1.SelectedItems[0].SubItems[3].Text = Globals.pUzrPassword;
                listView1.SelectedItems[0].SubItems[4].Text = Globals.pUzrRole;
       
            };
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;


            DialogResult dialogResult = MessageBox.Show("Do you want to delete current record with ID : " + listView1.SelectedItems[0].SubItems[0].Text, "Delete record", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Globals.pSqlString = ("Update Uzr SET deleted=1 where id=@id");
                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@id", listView1.SelectedItems[0].SubItems[0].Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                listView1.Items.Remove(listView1.SelectedItems[0]);
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
           

            Microsoft.Office.Interop.Excel.Application xla = new Microsoft.Office.Interop.Excel.Application();
            xla.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = xla.Workbooks.Add(Microsoft.Office.Interop.Excel.XlSheetType.xlWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)xla.ActiveSheet;
            int i = 2;
            int j = 1;

            for (int z = 0; z < listView1.Columns.Count; z++)
            {
                //MessageBox.Show(listView1.Columns[z].Text);
                ws.Cells[1, z + 1] = listView1.Columns[z].Text;
            }



            // i = 2;

            foreach (ListViewItem comp in listView1.Items)

            {
                ws.Cells[i, j] = comp.Text.ToString();
                //MessageBox.Show(comp.Text.ToString());
                foreach (ListViewItem.ListViewSubItem drv in comp.SubItems)
                {
                    ws.Cells[i, j] = drv.Text.ToString();
                    j++;
                }

                j = 1;

                i++;

            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                buttonDelete.Enabled = false;
                buttonUpdate.Enabled = false;
            };


            if (listView1.SelectedItems.Count == 1)
            {
                buttonDelete.Enabled = true;
                buttonUpdate.Enabled = true;
            };
        }

        private void Uzr_Load(object sender, EventArgs e)
        {

        }
    }
}
