﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class Order : Form
    {
        
        protected void ListviewResize(System.Windows.Forms.ListView pControl)
        {
            OrderRecord form = new OrderRecord();
            int pListviewWidth = pControl.Width;
            int pColumns = pControl.Columns.Count;

            int pOneColWidth = pListviewWidth / pColumns;

            for (int i = 0; i < pColumns - 1; i++)
            {
                pControl.Columns[i].Width = pOneColWidth;
            }

            pControl.Columns[0].Width = 50;
            pControl.Columns[pColumns - 1].Width = pControl.Columns[pColumns - 1].Width + pOneColWidth - 50;
        }

        protected void Listview2Resize(System.Windows.Forms.ListView pControl)
        {
            OrderRecord form = new OrderRecord();
            int pListviewWidth = pControl.Width;
            int pColumns = pControl.Columns.Count;

            int pOneColWidth = pListviewWidth / pColumns;

            for (int i = 0; i < pColumns - 1; i++)
            {
                pControl.Columns[i].Width = pOneColWidth;
            }

            pControl.Columns[0].Width = 50;
            pControl.Columns[pColumns - 1].Width = pControl.Columns[pColumns - 1].Width + pOneColWidth - 50;
        }

        public Order()
        {
           InitializeComponent();
           LoadListview1();
           LoadListview2();

            if (Globals.pUzrRole == "1")
            {
                buttonPrint.Enabled = true;
            }
            if (Globals.pUzrRole == "2")

            {
                buttonPrint.Enabled = false;

            }
        }

        private void LoadListview1()
        {

            Globals.pSqlString =
                "select orderz.id as orderz_id," +
                "customer.fullname as customer_fullname," +
                "car.model as car_model," +
                "orderz.taken_date," +
                "orderz.notes_book from orderz, customer,car " +
                "where orderz.status_id=1 and customer.id=orderz.customer_id and car.id=orderz.car_id and " +
                "orderz.deleted=0";


            listView1.Items.Clear();
            listView1.Refresh();
            listView1.View = View.Details;
            listView1.FullRowSelect = true;
            SqlConnection conn = new SqlConnection(Globals.connstr);
            SqlDataAdapter ada = new SqlDataAdapter(Globals.pSqlString, conn);
            DataTable dt = new DataTable();
            ada.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["orderz_id"].ToString());
                listitem.SubItems.Add(dr["customer_fullname"].ToString());
                listitem.SubItems.Add(dr["car_model"].ToString());
                //listitem.SubItems.Add(dr["taken_date"].ToString());
                listitem.SubItems.Add(dr["taken_date"].ToString());
                listitem.SubItems.Add(dr["notes_book"].ToString());

                 listView1.Items.Add(listitem);
            }
            if (dt.Rows.Count > 0)
            {
                listView1.Items[dt.Rows.Count - 1].Selected = true;
            }
            //toolStripStatusLabel1.Text = "Total record count : " + dt.Rows.Count.ToString();
        }

        private void LoadListview2()
        {

            Globals.pSqlString =
                "select orderz.id as orderz_id," +
                "customer.fullname as customer_fullname," +
                "car.model as car_model," +
                "orderz.given_date," +
                "orderz.taken_date," +
                " orderz.notes_return from orderz, customer,car " +
                " where orderz.status_id = 2 and " +
                " customer.id=orderz.customer_id and car.id=orderz.car_id and " +
                " orderz.deleted=0";

            listView2.Items.Clear();
            listView2.Refresh();
            listView2.View = View.Details;
            listView2.FullRowSelect = true;
            SqlConnection conn = new SqlConnection(Globals.connstr);
            SqlDataAdapter ada = new SqlDataAdapter(Globals.pSqlString, conn);
            DataTable dt = new DataTable();
            ada.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                ListViewItem listitem = new ListViewItem(dr["orderz_id"].ToString());
                listitem.SubItems.Add(dr["customer_fullname"].ToString());
                listitem.SubItems.Add(dr["car_model"].ToString());
                listitem.SubItems.Add(dr["taken_date"].ToString());
                listitem.SubItems.Add(dr["given_date"].ToString());
                listitem.SubItems.Add(dr["notes_return"].ToString());


                listView2.Items.Add(listitem);
            }
            if (dt.Rows.Count > 0)
            {
           //     listView1.Items[dt.Rows.Count - 1].Selected = true;
            }
            //toolStripStatusLabel1.Text = "Total record count : " + dt.Rows.Count.ToString();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

      

        
        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application xla = new Microsoft.Office.Interop.Excel.Application();
            xla.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = xla.Workbooks.Add(Microsoft.Office.Interop.Excel.XlSheetType.xlWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)xla.ActiveSheet;
            int i = 2;
            int j = 1;

            for (int z = 0; z < listView1.Columns.Count; z++)
            {
                //MessageBox.Show(listView1.Columns[z].Text);
                ws.Cells[1, z + 1] = listView1.Columns[z].Text;
            }



            // i = 2;

            foreach (ListViewItem comp in listView1.Items)

            {
                ws.Cells[i, j] = comp.Text.ToString();
                //MessageBox.Show(comp.Text.ToString());
                foreach (ListViewItem.ListViewSubItem drv in comp.SubItems)
                {
                    ws.Cells[i, j] = drv.Text.ToString();
                    j++;
                }

                j = 1;

                i++;

            }
        }

          
              

        private void listView1_Resize_1(object sender, EventArgs e)
        {
            ListviewResize(listView1);
        }

        private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_Resize_2(object sender, EventArgs e)
        {
            ListviewResize(listView1);

        }

        

        private void listView2_Resize(object sender, EventArgs e)
        {
            Listview2Resize(listView2);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OrderRecordReturn form = new OrderRecordReturn();
            Globals.pCancelAction = false;
            form.Text = "Insert record";
            Globals.pCancelAction = false;
            form.ShowDialog(this);
            if (Globals.pCancelAction == false)
            {
                LoadListview2();
            };
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Order_Load(object sender, EventArgs e)
        {

        }

        private void listView2_Resize_1(object sender, EventArgs e)
        {
            Listview2Resize(listView2);
        }

        private void listView1_Resize(object sender, EventArgs e)
        {
            Listview2Resize(listView1);
        }

        private void buttonClose_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonInsert_Click_1(object sender, EventArgs e)
        {
            OrderRecord form = new OrderRecord();
            Globals.pCancelAction = false;
            form.Text = "Insert record";
            Globals.pCancelAction = false;
            form.ShowDialog(this);
            if (Globals.pCancelAction == false)
            {
                LoadListview1();
                LoadListview2();
            };
        }

        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void returnTheCarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrderRecordReturn form = new OrderRecordReturn();
            Globals.pCancelAction = false;
            form.Text = "Insert record";
            Globals.pOrderId = listView1.SelectedItems[0].SubItems[0].Text;
            Globals.pCancelAction = false;
            form.ShowDialog(this);
            if (Globals.pCancelAction == false)
            {
                LoadListview1();
                LoadListview2();
            };
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonPrint_Click_1(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application xla = new Microsoft.Office.Interop.Excel.Application();
            xla.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook wb = xla.Workbooks.Add(Microsoft.Office.Interop.Excel.XlSheetType.xlWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)xla.ActiveSheet;
            int i = 2;
            int j = 1;

            for (int z = 0; z < listView1.Columns.Count; z++)
            {
                //MessageBox.Show(listView1.Columns[z].Text);
                ws.Cells[1, z + 1] = listView1.Columns[z].Text;
            }



            // i = 2;

            foreach (ListViewItem comp in listView1.Items)

            {
                ws.Cells[i, j] = comp.Text.ToString();
                //MessageBox.Show(comp.Text.ToString());
                foreach (ListViewItem.ListViewSubItem drv in comp.SubItems)
                {
                    ws.Cells[i, j] = drv.Text.ToString();
                    j++;
                }

                j = 1;

                i++;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OrderRecordReturn form = new OrderRecordReturn();
            Globals.pCancelAction = false;
            form.Text = "Insert record";
            Globals.pOrderId = listView1.SelectedItems[0].SubItems[0].Text;
            Globals.pCancelAction = false;
            form.ShowDialog(this);
            if (Globals.pCancelAction == false)
            {
                LoadListview1();
                LoadListview2();
            };
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
