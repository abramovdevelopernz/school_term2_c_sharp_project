﻿namespace Car
{
    partial class OrderRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paneMain = new System.Windows.Forms.Panel();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.RichTextBoxNotes = new System.Windows.Forms.RichTextBox();
            this.ComboBoxCar = new System.Windows.Forms.ComboBox();
            this.ComboBoxCustomer = new System.Windows.Forms.ComboBox();
            this.LabelPhone = new System.Windows.Forms.Label();
            this.LabelGender = new System.Windows.Forms.Label();
            this.LabelEmail = new System.Windows.Forms.Label();
            this.LabelFullName = new System.Windows.Forms.Label();
            this.buttonAction = new System.Windows.Forms.Button();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.paneMain.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // paneMain
            // 
            this.paneMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneMain.Controls.Add(this.dateTimePicker1);
            this.paneMain.Controls.Add(this.RichTextBoxNotes);
            this.paneMain.Controls.Add(this.ComboBoxCar);
            this.paneMain.Controls.Add(this.ComboBoxCustomer);
            this.paneMain.Controls.Add(this.LabelPhone);
            this.paneMain.Controls.Add(this.LabelGender);
            this.paneMain.Controls.Add(this.LabelEmail);
            this.paneMain.Controls.Add(this.LabelFullName);
            this.paneMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneMain.Location = new System.Drawing.Point(0, 0);
            this.paneMain.Name = "paneMain";
            this.paneMain.Size = new System.Drawing.Size(343, 282);
            this.paneMain.TabIndex = 17;
            this.paneMain.Paint += new System.Windows.Forms.PaintEventHandler(this.paneMain_Paint);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(129, 68);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 26;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // RichTextBoxNotes
            // 
            this.RichTextBoxNotes.Location = new System.Drawing.Point(129, 103);
            this.RichTextBoxNotes.Name = "RichTextBoxNotes";
            this.RichTextBoxNotes.Size = new System.Drawing.Size(200, 96);
            this.RichTextBoxNotes.TabIndex = 23;
            this.RichTextBoxNotes.Text = "";
            this.RichTextBoxNotes.TextChanged += new System.EventHandler(this.RichTextBoxNotes_TextChanged);
            // 
            // ComboBoxCar
            // 
            this.ComboBoxCar.FormattingEnabled = true;
            this.ComboBoxCar.Location = new System.Drawing.Point(129, 41);
            this.ComboBoxCar.Name = "ComboBoxCar";
            this.ComboBoxCar.Size = new System.Drawing.Size(200, 21);
            this.ComboBoxCar.TabIndex = 19;
            this.ComboBoxCar.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCar_SelectedIndexChanged);
            // 
            // ComboBoxCustomer
            // 
            this.ComboBoxCustomer.FormattingEnabled = true;
            this.ComboBoxCustomer.Location = new System.Drawing.Point(129, 14);
            this.ComboBoxCustomer.Name = "ComboBoxCustomer";
            this.ComboBoxCustomer.Size = new System.Drawing.Size(200, 21);
            this.ComboBoxCustomer.TabIndex = 18;
            this.ComboBoxCustomer.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCustomer_SelectedIndexChanged);
            // 
            // LabelPhone
            // 
            this.LabelPhone.AutoSize = true;
            this.LabelPhone.Location = new System.Drawing.Point(43, 103);
            this.LabelPhone.Name = "LabelPhone";
            this.LabelPhone.Size = new System.Drawing.Size(63, 13);
            this.LabelPhone.TabIndex = 17;
            this.LabelPhone.Text = "Book Notes";
            this.LabelPhone.Click += new System.EventHandler(this.LabelPhone_Click);
            // 
            // LabelGender
            // 
            this.LabelGender.AutoSize = true;
            this.LabelGender.Location = new System.Drawing.Point(42, 68);
            this.LabelGender.Name = "LabelGender";
            this.LabelGender.Size = new System.Drawing.Size(64, 13);
            this.LabelGender.TabIndex = 14;
            this.LabelGender.Text = "Taken Date";
            this.LabelGender.Click += new System.EventHandler(this.LabelGender_Click);
            // 
            // LabelEmail
            // 
            this.LabelEmail.AutoSize = true;
            this.LabelEmail.Location = new System.Drawing.Point(83, 41);
            this.LabelEmail.Name = "LabelEmail";
            this.LabelEmail.Size = new System.Drawing.Size(23, 13);
            this.LabelEmail.TabIndex = 13;
            this.LabelEmail.Text = "Car";
            this.LabelEmail.Click += new System.EventHandler(this.LabelEmail_Click);
            // 
            // LabelFullName
            // 
            this.LabelFullName.AutoSize = true;
            this.LabelFullName.Location = new System.Drawing.Point(55, 14);
            this.LabelFullName.Name = "LabelFullName";
            this.LabelFullName.Size = new System.Drawing.Size(51, 13);
            this.LabelFullName.TabIndex = 12;
            this.LabelFullName.Text = "Customer";
            this.LabelFullName.Click += new System.EventHandler(this.LabelFullName_Click);
            // 
            // buttonAction
            // 
            this.buttonAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAction.Location = new System.Drawing.Point(238, 6);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(90, 33);
            this.buttonAction.TabIndex = 3;
            this.buttonAction.Text = "Book";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBottom.Controls.Add(this.buttonAction);
            this.panelBottom.Controls.Add(this.buttonClose);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 282);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(343, 45);
            this.panelBottom.TabIndex = 16;
            this.panelBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBottom_Paint);
            // 
            // buttonClose
            // 
            this.buttonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonClose.Location = new System.Drawing.Point(3, 5);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(90, 33);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // OrderRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 327);
            this.Controls.Add(this.paneMain);
            this.Controls.Add(this.panelBottom);
            this.Name = "OrderRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderRecord";
            this.paneMain.ResumeLayout(false);
            this.paneMain.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel paneMain;
        private System.Windows.Forms.Label LabelPhone;
        private System.Windows.Forms.Label LabelGender;
        private System.Windows.Forms.Label LabelEmail;
        private System.Windows.Forms.Label LabelFullName;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.ComboBox ComboBoxCustomer;
        private System.Windows.Forms.ComboBox ComboBoxCar;
        private System.Windows.Forms.RichTextBox RichTextBoxNotes;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}