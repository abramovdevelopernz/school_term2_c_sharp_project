﻿namespace Car
{
    partial class ColorzRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelBottom = new System.Windows.Forms.Panel();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.paneMain = new System.Windows.Forms.Panel();
            this.TextBoxColorz = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelBottom.SuspendLayout();
            this.paneMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBottom
            // 
            this.panelBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBottom.Controls.Add(this.buttonAction);
            this.panelBottom.Controls.Add(this.buttonClose);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 132);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(330, 45);
            this.panelBottom.TabIndex = 14;
            this.panelBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.panelBottom_Paint);
            // 
            // buttonAction
            // 
            this.buttonAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAction.Location = new System.Drawing.Point(190, 5);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(90, 33);
            this.buttonAction.TabIndex = 3;
            this.buttonAction.Text = "Action";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonClose.Location = new System.Drawing.Point(3, 5);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(90, 33);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // paneMain
            // 
            this.paneMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paneMain.Controls.Add(this.TextBoxColorz);
            this.paneMain.Controls.Add(this.label3);
            this.paneMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneMain.Location = new System.Drawing.Point(0, 0);
            this.paneMain.Name = "paneMain";
            this.paneMain.Size = new System.Drawing.Size(330, 177);
            this.paneMain.TabIndex = 15;
            this.paneMain.Paint += new System.Windows.Forms.PaintEventHandler(this.paneMain_Paint);
            // 
            // TextBoxColorz
            // 
            this.TextBoxColorz.Location = new System.Drawing.Point(88, 63);
            this.TextBoxColorz.Name = "TextBoxColorz";
            this.TextBoxColorz.Size = new System.Drawing.Size(171, 20);
            this.TextBoxColorz.TabIndex = 15;
            this.TextBoxColorz.TextChanged += new System.EventHandler(this.TextBoxColorz_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Color";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // ColorzRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 177);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.paneMain);
            this.Name = "ColorzRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ColorzRecord";
            this.Load += new System.EventHandler(this.ColorzRecord_Load);
            this.panelBottom.ResumeLayout(false);
            this.paneMain.ResumeLayout(false);
            this.paneMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel paneMain;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxColorz;
    }
}