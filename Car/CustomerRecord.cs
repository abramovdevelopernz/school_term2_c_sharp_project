﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace Car
{
    public partial class CustomerRecord : Form
    {
        protected void LoadDropdownList(string pTableName, string pFieldName, System.Windows.Forms.ComboBox pControl)
        {
            //  string key = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Key;
            //  string value = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Value;
            //MessageBox.Show(key + ": " + value);
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            SqlConnection conn = new SqlConnection(Globals.connstr);
            conn.Open();
            string query = "select id," + pFieldName + " from " + pTableName + " order by id asc";
            SqlCommand cmd = new SqlCommand(query, conn);
            Globals.dr = cmd.ExecuteReader();


            if (Globals.dr.HasRows)
            {
                while (Globals.dr.Read())
                {
                    comboSource.Add(Globals.dr["id"].ToString(), Globals.dr[pFieldName].ToString());
                }
            }
            conn.Close();

            pControl.DataSource = new BindingSource(comboSource, null);
            pControl.DisplayMember = "Value";
            pControl.ValueMember = "Key";
            pControl.DropDownStyle = ComboBoxStyle.DropDownList;
            pControl.SelectedIndex = 0;
        }

        private void ClearAll()
        {
            LoadDropdownList("gender","name", comboBoxGender);
            TextBoxFullName.Clear();
            TextBoxEmail.Clear();
            TextBoxAddress.Clear();
            TextBoxPhone.Clear();

        }

        private bool CheckNullValues()
        {
            if (TextBoxFullName.Text == "")
            {
                MessageBox.Show("Field [Full Name] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxFullName.Focus();
                return false;
            }

            if (TextBoxEmail.Text == "")
            {
                MessageBox.Show("Field [Email] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxEmail.Focus();
                return false;
            }
            if (comboBoxGender.Text == " ")
            {
                MessageBox.Show("Field [Gender] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                comboBoxGender.Focus();
                return false;
            }
            if (dateTimePicker1.Text == "")
            {
                MessageBox.Show("Field [Date Of Birth] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dateTimePicker1.Focus();
                return false;
            }
            if (TextBoxAddress.Text == "")
            {
                MessageBox.Show("Field [Address] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxAddress.Focus();
                return false;
            }
            if (TextBoxPhone.Text == "")
            {
                MessageBox.Show("Field [Phone] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxPhone.Focus();
                return false;
            }
            else
            {
                return true;
            }

        }


        public CustomerRecord()
        {
            InitializeComponent();
            ClearAll();
            ControlBox = false;
        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {
                Regex emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

                if (emailRegex.IsMatch(TextBoxEmail.Text) == false)
                {
                    MessageBox.Show("Email field is in a wrong format");
                    TextBoxEmail.Focus();
                    return;
                }
                if (this.Text == "Insert record")
                {
                    Globals.pSqlString = ("INSERT INTO customer (" +
                        "fullname, email, gender_id, date_of_birth, address, phone) " +
                        "values (@fullname, @email, @gender_id, @date_of_birth, @address, @phone)");
                };

                if (this.Text == "Edit record")
                {
                    Globals.pSqlString = ("UPDATE customer SET gender_id=@gender_id, fullname= @fullname, email=@email, date_of_birth=@date_of_birth, address=@address, phone=@phone where id=" + Globals.pCustomerId);
                };

                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@fullname", TextBoxFullName.Text);
                cmd.Parameters.AddWithValue("@email", TextBoxEmail.Text);
                cmd.Parameters.AddWithValue("@gender_id", ((KeyValuePair<string, string>)comboBoxGender.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@date_of_birth", SqlDbType.Date).Value = dateTimePicker1.Value.Date;
                cmd.Parameters.AddWithValue("@address", TextBoxAddress.Text);
                cmd.Parameters.AddWithValue("@phone", TextBoxPhone.Text);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                if (this.Text == "Edit record")
                {
                    Globals.pCustomerFullname = TextBoxFullName.Text;
                    Globals.pCustomerEmail = TextBoxEmail.Text;
                    Globals.pCustomerGenderId = ((KeyValuePair<string, string>)comboBoxGender.SelectedItem).Value;
                    Globals.pCustomerDateOfBirth = dateTimePicker1.Text;
                    Globals.pCustomerAddress = TextBoxAddress.Text;
                    Globals.pCustomerPhone = TextBoxPhone.Text;

                }

                Close();
            }

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Globals.pCancelAction = true;
            Close();
        }

        private void LabelDateOfBirth_Click(object sender, EventArgs e)
        {

        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CustomerRecord_Load(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from customer where id = " + Globals.pCustomerId, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    TextBoxFullName.Text = dt.Rows[0]["fullname"].ToString();
                    TextBoxEmail.Text = dt.Rows[0]["email"].ToString();
                    comboBoxGender.SelectedValue = dt.Rows[0]["gender_id"].ToString();
                    dateTimePicker1.Text = dt.Rows[0]["date_of_birth"].ToString();
                    TextBoxAddress.Text = dt.Rows[0]["address"].ToString();
                    TextBoxPhone.Text = dt.Rows[0]["phone"].ToString();
                }

            };
        }

        private void TextBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }

        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TextBoxFullName_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxDateOfBirth_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxGender_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void LabelPhone_Click(object sender, EventArgs e)
        {

        }

        private void LabelAddress_Click(object sender, EventArgs e)
        {

        }

        private void LabelGender_Click(object sender, EventArgs e)
        {

        }

        private void LabelEmail_Click(object sender, EventArgs e)
        {

        }

        private void LabelFullName_Click(object sender, EventArgs e)
        {

        }
    }
}
