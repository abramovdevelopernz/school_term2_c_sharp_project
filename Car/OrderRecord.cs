﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace Car
{
    public partial class OrderRecord : Form
    {



        protected void LoadDropdownListCar()
        {
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            SqlConnection conn = new SqlConnection(Globals.connstr);
            conn.Open();

            string query =
                    "select car.id as car_id" +
                    ",makez.makez,car.model," +
                    "colorz.colorz," +
                    "typez.name," +
                    "car.year," +
                    "car.notes " +
                    "from car,makez,colorz,typez where makez.id=" +
                    "car.makez_id and colorz.id=car.colorz_id and typez.id=car.typez_id and car.deleted=0 "+
                    "and car.id not in (Select orderz.car_id as car_id from orderz where deleted=0 and status_id=1)";
            
            SqlCommand cmd = new SqlCommand(query, conn);
            Globals.dr = cmd.ExecuteReader();

            if (Globals.dr.HasRows)
            {
                while (Globals.dr.Read())
                {
                    comboSource.Add(Globals.dr["car_id"].ToString(), Globals.dr["car_id"].ToString()+" - " + Globals.dr["colorz"].ToString() +" - "+ Globals.dr["model"].ToString());
                }


                ComboBoxCar.DataSource = new BindingSource(comboSource, null);
                ComboBoxCar.DisplayMember = "Value";
                ComboBoxCar.ValueMember = "Key";
                ComboBoxCar.DropDownStyle = ComboBoxStyle.DropDownList;
                ComboBoxCar.SelectedIndex = 0;


            }
            conn.Close();




        }










        protected void LoadDropdownList(string pTableName, string pFieldName, System.Windows.Forms.ComboBox pControl)
        {
            //  string key = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Key;
            //  string value = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Value;
            //MessageBox.Show(key + ": " + value);
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            SqlConnection conn = new SqlConnection(Globals.connstr);
            conn.Open();
            string query = "select id," + pFieldName + " from " + pTableName + " where deleted=0  order by id asc";
            SqlCommand cmd = new SqlCommand(query, conn);
            Globals.dr = cmd.ExecuteReader();


            if (Globals.dr.HasRows)
            {
                while (Globals.dr.Read())
                {
                    comboSource.Add(Globals.dr["id"].ToString(), Globals.dr[pFieldName].ToString());
                }


                pControl.DataSource = new BindingSource(comboSource, null);
                pControl.DisplayMember = "Value";
                pControl.ValueMember = "Key";
                pControl.DropDownStyle = ComboBoxStyle.DropDownList;
                pControl.SelectedIndex = 0;


            }
            conn.Close();




        }
        private void ClearAll()
        {
            LoadDropdownList("customer", "fullname", ComboBoxCustomer);
            // LoadDropdownList("car", "model", ComboBoxCar);
            LoadDropdownListCar();
            RichTextBoxNotes.Clear();
        }

        private bool CheckNullValues()
        {
            if (ComboBoxCustomer.Text == " " || ComboBoxCustomer.Text == "-")
            {
                MessageBox.Show("Field [Customer] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ComboBoxCustomer.Focus();
                return false;
            }

            if (ComboBoxCar.Text == " " || ComboBoxCar.Text == "-")
            {
                MessageBox.Show("Field [Car] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ComboBoxCar.Focus();
                return false;
            }
            if (dateTimePicker1.Text == "")
            {
                MessageBox.Show("Field [Taken Date] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dateTimePicker1.Focus();
                return false;
            }
            
            
            if (RichTextBoxNotes.Text == "")
            {
                MessageBox.Show("Field [Notes] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RichTextBoxNotes.Focus();
                return false;
            }
            else
            {
                return true;
            }

        }
        public OrderRecord()
        {
            InitializeComponent();
            ClearAll();
            ControlBox = false;

        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {



                    Globals.pSqlString = ("INSERT INTO orderz " +
                        "(customer_id," +
                        "car_id," +
                        "taken_date," +
                        "status_id," +
                        "notes_book) values " +
                        "(@customer_id, " +
                        "@car_id," +
                        "@taken_date," +
                        "1," +
                        " @notes_book)");

                
                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@customer_id", ((KeyValuePair<string, string>)ComboBoxCustomer.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@car_id", ((KeyValuePair<string, string>)ComboBoxCar.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@taken_date", SqlDbType.Date).Value = dateTimePicker1.Value.Date;
                cmd.Parameters.AddWithValue("@notes_book", RichTextBoxNotes.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                

                Close();
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Globals.pCancelAction = true;
            Close();
        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void RichTextBoxNotes_TextChanged(object sender, EventArgs e)
        {

        }

        private void ComboBoxCar_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ComboBoxCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LabelPhone_Click(object sender, EventArgs e)
        {

        }

        private void LabelGender_Click(object sender, EventArgs e)
        {

        }

        private void LabelEmail_Click(object sender, EventArgs e)
        {

        }

        private void LabelFullName_Click(object sender, EventArgs e)
        {

        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
