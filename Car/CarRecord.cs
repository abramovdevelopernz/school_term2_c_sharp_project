﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class CarRecord : Form
    {

        protected void LoadDropdownList(string pTableName, string pFieldName, System.Windows.Forms.ComboBox pControl)
        {
            //  string key = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Key;
            //  string value = ((KeyValuePair<string, string>)comboBox1.SelectedItem).Value;
            //MessageBox.Show(key + ": " + value);
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            SqlConnection conn = new SqlConnection(Globals.connstr);
            conn.Open();
            string query = "select id," + pFieldName + " from " + pTableName + " where deleted=0  order by id asc";
            SqlCommand cmd = new SqlCommand(query, conn);
            Globals.dr = cmd.ExecuteReader();


            if (Globals.dr.HasRows)
            {
                while (Globals.dr.Read())
                {
                    comboSource.Add(Globals.dr["id"].ToString(), Globals.dr[pFieldName].ToString());
                }


                pControl.DataSource = new BindingSource(comboSource, null);
                pControl.DisplayMember = "Value";
                pControl.ValueMember = "Key";
                pControl.DropDownStyle = ComboBoxStyle.DropDownList;
                pControl.SelectedIndex = 0;


            }
            conn.Close();


    
            
        }

        private void ClearAll()
        {
            LoadDropdownList("makez", "makez", comboBoxMakez);
            LoadDropdownList("colorz", "colorz", comboBoxColorz);
            LoadDropdownList("typez", "name", comboBoxTypez);
            textBoxModel.Clear();
            textBoxYear.Clear();
            richTextBoxNotes.Clear();
            textBoxModel.Focus();
        }

        private bool CheckNullValues()
        {
            if (comboBoxMakez.Text == "-" || comboBoxMakez.Text == "")
            {
                MessageBox.Show("Field [Make] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxModel.Focus();
                return false;
            }

            if (textBoxModel.Text == "")
            {
                MessageBox.Show("Field [Model] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxModel.Focus();
                return false;
            }

            if (comboBoxColorz.Text == "-" || comboBoxColorz.Text == "")
            {
                MessageBox.Show("Field [Color] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxModel.Focus();
                return false;
            }

            if (comboBoxTypez.Text == "-" || comboBoxTypez.Text == "")
            {
                MessageBox.Show("Field [Type] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxModel.Focus();
                return false;
            }

            if (textBoxYear.Text == "")
            {
                MessageBox.Show("Field [Year] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxYear.Focus();
                return false;
            }

            else
            {
                return true;
            }

        }

        private void CarRecord_Activated(object sender, System.EventArgs e)
        {
           
        }

        public CarRecord()
        {
            InitializeComponent();
            ControlBox = false;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Globals.pCancelAction = true;
            Close();
        }

        private void richTextBoxNotes_TextChanged(object sender, EventArgs e)
        {

        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CarRecord_Load(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from car where id = "+ Globals.pCarId, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    comboBoxMakez.SelectedValue = dt.Rows[0]["makez_id"].ToString();
                    comboBoxColorz.SelectedValue = dt.Rows[0]["colorz_id"].ToString();
                    comboBoxTypez.SelectedValue = dt.Rows[0]["typez_id"].ToString();
                    textBoxModel.Text = dt.Rows[0]["model"].ToString();
                    textBoxYear.Text = dt.Rows[0]["year"].ToString();
                    richTextBoxNotes.Text = dt.Rows[0]["notes"].ToString();
                }

            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
        
        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {


                if (this.Text == "Insert record")
                {
                    Globals.pSqlString = ("INSERT INTO car (makez_id,model,colorz_id,typez_id,year,notes) values (@makez_id, @model, @colorz_id, @typez_id, @year, @notes)");
                };

                if (this.Text == "Edit record")
                {
                    Globals.pSqlString = ("UPDATE car SET makez_id=@makez_id, model = @model,colorz_id=@colorz_id,typez_id=@typez_id,year=@year,notes=@notes where id="+Globals.pCarId);
                };

                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@makez_id", ((KeyValuePair<string, string>)comboBoxMakez.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@model", textBoxModel.Text);
                cmd.Parameters.AddWithValue("@colorz_id", ((KeyValuePair<string, string>)comboBoxColorz.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@typez_id", ((KeyValuePair<string, string>)comboBoxTypez.SelectedItem).Key);
                cmd.Parameters.AddWithValue("@year", textBoxYear.Text);
                cmd.Parameters.AddWithValue("@notes", richTextBoxNotes.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                if (this.Text == "Edit record")
                {
                    Globals.pCarMakez = ((KeyValuePair<string, string>)comboBoxMakez.SelectedItem).Value;
                    Globals.pCarModel = textBoxModel.Text;
                    Globals.pCarColorz = ((KeyValuePair<string, string>)comboBoxColorz.SelectedItem).Value;
                    Globals.pCarTypez = ((KeyValuePair<string, string>)comboBoxTypez.SelectedItem).Value;
                    Globals.pCarYear = textBoxYear.Text;
                    Globals.pCarNotes = richTextBoxNotes.Text;
                };

                Close();
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxYear_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxTypez_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxColorz_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBoxModel_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxMakez_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
