﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car

{

    
    public partial class TypeRecord : Form
    {

        private bool CheckNullValues()
        {
            if (TextBoxTypez.Text == "")
            {
                MessageBox.Show("Field [Type] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxTypez.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        private void ClearAll()
        {

            TextBoxTypez.Clear();

        }
        public TypeRecord()
        {
            InitializeComponent();
            ClearAll();
            ControlBox = false;

        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
                {
                    if (this.Text == "Insert record")
                    {
                        Globals.pSqlString = ("INSERT INTO typez (name) values (@name)");
                    };

                    if (this.Text == "Edit record")
                    {
                        Globals.pSqlString = ("INSERT INTO typez (name) values (@name)");
                    };

                    SqlConnection conn = new SqlConnection(Globals.connstr);
                    SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                    cmd.Parameters.AddWithValue("@name", TextBoxTypez.Text);


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();


                    if (this.Text == "Edit record")
                    {
                        Globals.pTypeName = TextBoxTypez.Text;
                    }

                    Close();
                }
            }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
