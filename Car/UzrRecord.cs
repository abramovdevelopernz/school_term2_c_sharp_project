﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace Car
{
    public partial class UzrRecord : Form
    {
        private void ClearAll()
        {
            TextBoxName.Clear();
            TextBoxEmail.Clear();
            TextBoxPassword.Clear();
        }

        private bool CheckNullValues()
        {

            if (TextBoxName.Text == "")
            {
                MessageBox.Show("Field [Name] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxName.Focus();
                return false;
            }


            if (TextBoxEmail.Text == "")
            {
                MessageBox.Show("Field [Email] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxEmail.Focus();
                return false;
            }

            if (TextBoxPassword.Text == "")
            {
                MessageBox.Show("Field [Password] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxPassword.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        public UzrRecord()
        {
            InitializeComponent();
            ControlBox = false;
            ClearAll();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UzrRecord_Load(object sender, EventArgs e)
        {
            
        }
                

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Globals.pCancelAction = true;
            Close();
        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {


                if (radioButton1.Checked) { Globals.pUzrRoleID = "1"; };
                if (radioButton2.Checked) { Globals.pUzrRoleID = "2"; };




                if (this.Text == "Insert record")
                {
                    Globals.pSqlString = ("INSERT INTO Uzr (name, email, pass, rolez) values (@name, @email, @pass, @rolez)");
                };

                if (this.Text == "Edit record")
                {
                    Globals.pSqlString = ("UPDATE Uzr SET name=@name, email=@email, pass=@pass, rolez=@rolez where id=" + Globals.pUzrID);
                };

                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@name", TextBoxName.Text);
                cmd.Parameters.AddWithValue("@email", TextBoxEmail.Text);
                cmd.Parameters.AddWithValue("@pass", TextBoxPassword.Text);
                cmd.Parameters.AddWithValue("@rolez", Globals.pUzrRoleID);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                if (this.Text == "Edit record")
                {
                    Globals.pUzrName = TextBoxName.Text;
                    Globals.pUzrEmail = TextBoxEmail.Text;
                    Globals.pUzrPassword = TextBoxEmail.Text;
                    if (Globals.pUzrRoleID == "1")
                    {
                        Globals.pUzrRole = "Administrator";

                    };


                    if (Globals.pUzrRoleID == "2") 
                    {
                        Globals.pUzrRole = "Assistant";

                    };



                };

                Close();
            }

            
        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UzrRecord_Load_1(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from Uzr where id = " + Globals.pUzrID, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    TextBoxName.Text = dt.Rows[0]["name"].ToString();
                    TextBoxEmail.Text = dt.Rows[0]["email"].ToString();
                    TextBoxPassword.Text = dt.Rows[0]["pass"].ToString();

                    if (dt.Rows[0]["rolez"].ToString() == "1")
                    {
                        radioButton1.Checked = true;
                    }
                    if (dt.Rows[0]["rolez"].ToString() == "2")
                    {
                        radioButton2.Checked = true;
                    }

                }
            }
        }
    }
}