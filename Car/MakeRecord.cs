﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class MakeRecord : Form
    {

        private bool CheckNullValues()
        {
            if (TextBoxMake.Text == "")
            {
                MessageBox.Show("Field [Make] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxMake.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        private void ClearAll()
        {

            TextBoxMake.Clear();

        }
        public MakeRecord()
        {
            InitializeComponent();
            ClearAll();
            ControlBox = false;


        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {
                if (CheckNullValues())
                {
                    if (this.Text == "Insert record")
                    {
                        Globals.pSqlString = ("INSERT INTO makez (makez) values (@makez_makez)");
                    };

                    if (this.Text == "Edit record")
                    {
                        Globals.pSqlString = ("INSERT INTO makez (makez) values (@makez_makez)");
                    };

                    SqlConnection conn = new SqlConnection(Globals.connstr);
                    SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                    cmd.Parameters.AddWithValue("@makez_makez", TextBoxMake.Text);


                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();


                    if (this.Text == "Edit record")
                    {
                        Globals.pMakeName = TextBoxMake.Text;
                    }

                    Close();
                }

            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void MakeRecord_Load(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from makez where id = " + Globals.pMakeId, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    TextBoxMake.Text = dt.Rows[0]["makez"].ToString();

                }
            }
        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TextBoxMake_TextChanged(object sender, EventArgs e)
        {

        }

        private void LabelMake_Click(object sender, EventArgs e)
        {

        }

        private void MakeRecord_Load_1(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from makez where id = " + Globals.pCustomerId, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    TextBoxMake.Text = dt.Rows[0]["makez"].ToString();

                }

            };
        }

        private void buttonClose_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
