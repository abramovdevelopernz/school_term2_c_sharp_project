﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Car
{
    public partial class ColorzRecord : Form
    {
        private bool CheckNullValues()
        {
            if (TextBoxColorz.Text == "")
            {
                MessageBox.Show("Field [Color] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxColorz.Focus();
                return false;
            }
            else
            {
                return true;
            }

        }
        private void ClearAll()
        {

            TextBoxColorz.Focus();
        }

        public ColorzRecord()
        {
            InitializeComponent();
            ControlBox = false;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAction_Click(object sender, EventArgs e)
        {
            if (CheckNullValues())
            {


                if (this.Text == "Insert record")
                {
                    Globals.pSqlString = ("INSERT INTO colorz (colorz) values (@colorz_colorz)");
                };

                if (this.Text == "Edit record")
                {
                    Globals.pSqlString = ("UPDATE colorz SET colorz.colorz= @colorz_colorz where id=" + Globals.pColorzId);
                };

                SqlConnection conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand(Globals.pSqlString, conn);
                cmd.Parameters.AddWithValue("@colorz_colorz", TextBoxColorz.Text);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                if (this.Text == "Edit record")
                {
                    Globals.pColorzName = TextBoxColorz.Text;
                };

                Close();
            }

        }

        private void ColorzRecord_Load(object sender, EventArgs e)
        {
            ClearAll();

            if (this.Text == "Insert record")
            {
                buttonAction.Text = "Insert";

            };

            if (this.Text == "Edit record")
            {
                buttonAction.Text = "Update";

                Globals.conn = new SqlConnection(Globals.connstr);
                SqlCommand cmd = new SqlCommand("SELECT * from colorz where id = " + Globals.pColorzId, Globals.conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    TextBoxColorz.Text = dt.Rows[0]["Colorz"].ToString();
                }

            };
        }

        private void panelBottom_Paint(object sender, PaintEventArgs e)
        {

        }

        private void paneMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TextBoxColorz_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
