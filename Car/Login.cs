﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace Car
{
    public partial class Login : Form
    {
        private bool CheckNullValues()
        {
            if (TextBoxEmail.Text == "")
            {
                TextBoxEmail.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }
        public Login()
        {
            InitializeComponent();
            // this.FormBorderStyle = FormBorderStyle.None;
            ControlBox = false;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            //  Regex emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

            //  if (emailRegex.IsMatch(TextBoxEmail.Text) == false)
            //    {
            //     MessageBox.Show("Email field is in a wrong format");
            //      TextBoxEmail.Focus();
            //    return;
            //   }

            if (TextBoxEmail.Text == "")
            {
                MessageBox.Show("Field [Email] can not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxEmail.Focus();
            }

            SqlConnection conn = new SqlConnection(Globals.connstr);
            SqlCommand cmd = new SqlCommand("SELECT * from uzr where email = '" + TextBoxEmail.Text + "' and pass = '" + TextBoxPassword.Text + "'", conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                Globals.pUzrID = dt.Rows[0]["id"].ToString();
                Globals.pUzrRole = dt.Rows[0]["rolez"].ToString();
                Close();
            }
            else
            {


            }

        }

        private void TextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void TextBoxEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (CheckNullValues())
            {
                if (e.KeyCode == Keys.Enter)
                {
                    TextBoxPassword.Focus();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
} 
